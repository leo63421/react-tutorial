import { Menu } from "@mui/icons-material";
import { AppBar, Box, IconButton, Toolbar, Typography } from "@mui/material";

// TODO: 1.4. simular with 1.1~1.3, get props, make IconButton onClick function to open sidebar
function Appbar() {
  return (
    <Box>
      <AppBar position="static" >
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <Menu />
          </IconButton>
        <Typography variant="h6" component="div">
          Tasty Restaurant
        </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  )
}

export default Appbar;