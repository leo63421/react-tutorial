import { ChevronLeft } from "@mui/icons-material";
import { Divider, Drawer, IconButton, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Toolbar } from "@mui/material";
import AccessibleForwardIcon from '@mui/icons-material/AccessibleForward';

// TODO: 1.1 get some props for this component to work with
function Sidebar() {
  const drawerWidth = 240;
  return (
    <Drawer
      anchor="left"
      // TODO: 1.2: make open depense on props
      open={true}
      variant="persistent"
      sx={{
        width: drawerWidth,
        '& .MuiDrawer-paper': {
          width: drawerWidth,
          boxSizing: 'border-box',
        },
      }}>

      <Toolbar sx={{ justifyContent: "flex-end" }}>
        {/* TODO: 1.3 onClick close menu */}
        <IconButton>
          <ChevronLeft />
        </IconButton>
      </Toolbar>

      <Divider />
      <List>
        <ListItem disablePadding>
          <ListItemButton>
            <ListItemIcon>
              <AccessibleForwardIcon />
            </ListItemIcon>
            <ListItemText>
              Address
            </ListItemText>
          </ListItemButton>
        </ListItem>
      </List>


    </Drawer>

  )
}

export default Sidebar;