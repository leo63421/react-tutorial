import './App.css';
import { BrowserRouter as Router, Link, Route, Routes } from 'react-router-dom'
import { Button } from '@mui/material'
import MainPage from './Pages/MainPage';
import AddressPage from './Pages/AddressPage';
import Appbar from './Components/Appbar';
import Sidebar from './Components/Sidebar';

function App() {

  // TODO: 1.0. add logic for open and close sidebar, hint: useState

  return (
    <Router>
      <Appbar />
      <Sidebar />
      {/* TODO: 2.0 move this Link to appbar Tyopgraphy */}
      <Link to='/'>
        <Button variant='outlined'> Main Page </Button>
      </Link>
      {/* TODO: 2.1 move this Link to sidebar ListItemButton */}
      <Link to='address'>
        <Button variant='contained'> Address Page </Button>
      </Link>
      <div>
        <Routes>
          <Route path='/' element={<MainPage />} />
          <Route path='/address' element={<AddressPage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
