import { Button, Card, CardActions, CardContent, CardMedia, Typography } from "@mui/material";

function MainPage() {
  return (
    <>
      <Card sx={{ maxWidth: 345 }}>
        <CardMedia
          component="img"
          height="140"
          image="/asset/Lasagna.jpg"
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            Lasagna
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Super tasty lasagna!!!!!
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small">Share</Button>
          <Button size="small">Learn More</Button>
        </CardActions>
      </Card>
    </>
  )
}

export default MainPage;