import { ChevronLeft } from "@mui/icons-material";
import { Divider, Drawer, IconButton, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Toolbar } from "@mui/material";
import AccessibleForwardIcon from '@mui/icons-material/AccessibleForward';
import { useNavigate } from "react-router-dom";

function Sidebar({ isOpen, iconButtonAction }) {
  const drawerWidth = 240;

  const navigate = useNavigate();

  return (
    <Drawer
      anchor="left"
      open={isOpen}
      variant="persistent"
      sx={{
        width: drawerWidth,
        '& .MuiDrawer-paper': {
          width: drawerWidth,
          boxSizing: 'border-box',
        },
      }}>

      <Toolbar sx={{ justifyContent: "flex-end" }}>
        <IconButton onClick={() => { iconButtonAction(false) }}>
          <ChevronLeft />
        </IconButton>
      </Toolbar>

      <Divider />
      <List>
        <ListItem disablePadding>
          <ListItemButton onClick={() => navigate('/address')}>
            <ListItemIcon>
              <AccessibleForwardIcon />
            </ListItemIcon>
            <ListItemText>
              Address
            </ListItemText>
          </ListItemButton>
        </ListItem>
      </List>

    </Drawer>

  )
}

export default Sidebar;