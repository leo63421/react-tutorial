import { Menu } from "@mui/icons-material";
import { AppBar, Box, IconButton, Toolbar, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";

function Appbar({ iconButtonAction }) {

  const navigate = useNavigate();

  return (
    <Box>
      <AppBar position="static" >
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            onClick={() => iconButtonAction(true)}
          >
            <Menu />
          </IconButton>
          <Typography variant="h6" component="div" onClick={() => navigate('/')}>
            Tasty Restaurant
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  )
}

export default Appbar;