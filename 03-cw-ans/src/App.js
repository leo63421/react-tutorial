import './App.css';
import React, { useState } from 'react';
import { BrowserRouter as Router, Link, Route, Routes } from 'react-router-dom'
import { Button } from '@mui/material'
import MainPage from './Pages/MainPage';
import AddressPage from './Pages/AddressPage';
import Appbar from './Components/Appbar';
import Sidebar from './Components/Sidebar';

function App() {

  const [isShowSideBar, setShowSideBar] = useState(true);

  return (
    <Router>
      <Appbar iconButtonAction={setShowSideBar} />
      <Sidebar isOpen={isShowSideBar} iconButtonAction={setShowSideBar} />
      <Link to='address'>
        <Button variant='contained'> Address Page </Button>
      </Link>
      <div>
        <Routes>
          <Route path='/' element={<MainPage />} />
          <Route path='/address' element={<AddressPage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
