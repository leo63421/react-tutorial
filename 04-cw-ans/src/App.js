import { useState, useEffect } from 'react';
import './App.css';
// TODO: 1. import axios
import axios from 'axios';

function App() {

  const [id, setId] = useState(1);
  const [postTitle, setPostTitle] = useState("");

  // TODO: 2. make GET request on componet mount (hint: useEffect)
  // TODO: 3.0. make GET request depense on variable (hint: useState)
  // useEffect(() => {
  //   axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
  //     .then(res => {
  //       console.log(res);
  //     })
  //     .catch(err => {
  //       console.log("ERROR: ", err)
  //     })
  // }, []);

  // TODO: 3.3. onClick function that also change display (hint: useState)
  function getRequest() {
    axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then(res => {
        console.log(res);
        setPostTitle(res.data.title);
      })
      .catch(err => {
        console.log("ERROR: ", err)
      })
  }

  // TODO: 4. Try POST request
  function postRequest() {
    axios.post(`https://jsonplaceholder.typicode.com/posts`, {
      title: 'New Title',
      body: 'asdlvknmsthoijreth',
      userId: 678
    })
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log("ERROR: ", err)
      })
  }

  return (
    <div>
      {/* TODO: 3.1. Input text */}
      <input type="text" value={id} onChange={(e) => { setId(e.target.value) }} />
      {/* TODO: 3.2. Button with onClick to make GET request */}
      <button onClick={() => getRequest()}> GET REQUEST! </button>
      {/* TODO: 3.4. Post */}
      <h3>{postTitle}</h3>
      <button onClick={() => postRequest()}> POST REQUEST! </button>
    </div>
  );
}

export default App;
