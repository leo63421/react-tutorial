import './App.css';
import { BrowserRouter as Router, Link, Route, Routes } from 'react-router-dom'
import Home from './Pages/Home';
import Settings from './Pages/Settings';
import { LightModeContext } from './Context';
import { useState } from 'react';

function App() {

  // TODO 3: Make useState that use for context provider
  const [isLightMode, setIsLightMode] = useState(true);

  return (
    <Router>
      <div className="topBar">
        <Link to='/'>
          <button>Home</button>
        </Link>
        <Link to='/settings'>
          <button>Settings</button>
        </Link>
      </div>

      {/* TODO 2: Make these page can use context */}
      <LightModeContext.Provider value={{ isLightMode, setIsLightMode }}>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/settings' element={<Settings />} />
        </Routes>
      </LightModeContext.Provider >
    </Router>
  );

  /*
    Homework: 
    Make a shopping cart:
    1. Make a item page
      1.1. Contain list of items
      1.2. Make a button to add that item to shopping cart
    2. Make a shopping cart page
      2.1. List all items in cart
      2.2. Add button to remove ALL items in cart

    Extra:
      Add button for each items, to remove that item in cart
      Hint 1. array.map work in jsx, i.e.
      <div>
        {
          array.map(item => {
            return <div>
              {item}
            </div>
          })
        }
      </div>
      Hint 2. array.map((item, index) => {...})
        make good use of index
      Hint 3. https://stackoverflow.com/questions/5767325/how-can-i-remove-a-specific-item-from-an-array
  */
}

export default App;
