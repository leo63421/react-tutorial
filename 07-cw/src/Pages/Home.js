import { useContext } from "react";
import { LightModeContext } from "../Context";

function Home() {

  // TODO 7: connect this page with context hint: repeat 4~5
  const { isLightMode, setIsLightMode } = useContext(LightModeContext);

  let modeString = "";

  if (isLightMode) {
    modeString = "lightDiv";
  } else {
    modeString = "darkDiv";
  }

  return (
    <>
      <div className={modeString}>
        Home Page
        {/* ... */}
      </div>
    </>
  )
}

export default Home;