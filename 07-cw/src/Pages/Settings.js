import { useContext } from "react";
import { LightModeContext } from "../Context";

function Settings() {
  // TODO 4: Connect this to context
  const { isLightMode, setIsLightMode } = useContext(LightModeContext);

  let modeString = "";

  if (isLightMode) {
    modeString = "lightDiv";
  } else {
    modeString = "darkDiv";
  }

  //                     condition  ? true return : false return
  // const modeString = isLightMode ? "lightDiv" : "darkDiv";

  return (
    <>
      {/* TODO 5: Make this div connect with light/dark mode hint: replace className with variable */}
      <div className={modeString}>
        <div>
          Settins Page
        </div>
        {/* TODO 6: Make toggle button for toggling Light<->Dark mode, hint: make function before return */}
        <button onClick={() => setIsLightMode(true)}> Light Mode! </button>
        <button onClick={() => setIsLightMode(false)}> Dark Mode! </button>

        {/* In darkMode: -> true ; In lightMode: -> false */}
        <button onClick={() => setIsLightMode(!isLightMode)}> Toggle Mode! </button>
      </div>
    </>
  )
}

export default Settings;