import './App.css';

function App() {

  const ppl = [
    { name: "Peter", gender: "M", age: 18, height: 1.7, weight: 62, glasses: true, socialCredit: 5562 },
    { name: "John", gender: "M", age: 25, height: 1.56, weight: 98, glasses: false, socialCredit: 83146 },
    { name: "Mary", gender: "F", age: 15, height: 1.52, weight: 50, glasses: false, socialCredit: 6412 },
    { name: "David", gender: "M", age: 30, height: 1.65, weight: 55, glasses: true, socialCredit: 0 },
    { name: "Sam", gender: "M", age: 70, height: 1.62, weight: 70, glasses: true, socialCredit: 513 },
    { name: "Amy", gender: "F", age: 53, height: 1.73, weight: 45, glasses: true, socialCredit: -321 },
  ]

  // // filter
  // const arr2 = arr.filter((item) => {
  //   return item < 10;
  // };

  // // map
  // arr.map((item) => {
  //   console.log(item);
  // });

  // // reduce
  // const sum = arr.reduce((prev, currentItem) => {
  //   return prev + currentItem;
  // }, 0);

  // TODO 1: Array of all male
  const male = ppl.filter((people) => {
    return people.gender === "M";
  });

  console.log("male:", male);

  // TODO 2: Array of person who older than 18
  const legal = ppl.filter((people) => {
    return people.age > 18;
  });

  console.log("legal:", legal);

  // TODO 3: Calculate BMI of each person (BMI = weight / height^2)
  const bmiCal = ppl.map((people) => {
    return people.weight / (people.height * people.height);
  })

  console.log("BMI:", bmiCal);

  // TODO 4: Get average age
  const totalAge = ppl.reduce((prev, currentPeople) => {
    return prev + currentPeople.age;
  }, 0);
  const totalPpl = ppl.length;
  console.log("Average age:", totalAge / totalPpl);

  // TODO 5: Console.log show name of all no glasses
  // method 1
  // filter
  const arr5a = ppl.filter((people) => {
    return people.glasses === false;
  });
  console.log("no glasses", arr5a);
  // mapping
  const arr5b = arr5a.map((people) => {
    console.log(people.name);
  });

  // method 2
  ppl.map((people) => {
    if (people.glasses == false) {
      console.log("Method 2:", people.name);
    }
  });

  const newArray = ppl.map((people) => {
    return {
      name: people.name,
      isGoodCitizen: people.socialCredit > 5000,
    }
  })

  console.log(newArray);

  // = = = = = = = = = = = = = = Homework = = = = = = = = = = = = = = 
  // TODO 1: Average weight of all people with glasses
  // TODO 2: Average BMI
  // TODO 3: Name of highest social credit
  // = = = = = = = = = = = = = = Homework = = = = = = = = = = = = = = 

  return (
    <div className="App">
      <div> Hello World</div>
    </div>
  );
}

export default App;
