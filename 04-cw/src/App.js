import { useState, useEffect } from 'react';
import './App.css';
// TODO: 1. import axios

function App() {

  // TODO: 2. make GET request on componet mount (hint: useEffect)
  // TODO: 3.0. make GET request depense on variable (hint: useState)

  // TODO: 3.3. onClick function that also change display (hint: useState)

  // TODO: 4. Try POST request

  return (
    <div>
      {/* TODO: 3.1. Input text */}
      {/* TODO: 3.2. Button with onClick to make GET request */}
      {/* TODO: 3.4. Post */}
    </div>
  );
}

export default App;
