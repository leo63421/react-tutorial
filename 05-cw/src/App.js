import axios from 'axios';
import './App.css';
import { useEffect, useState } from 'react';

const GOOD_URL = "https://jsonplaceholder.typicode.com/posts/10";
const BAD_URL = "https://jsonplaceholder.typicode.com/posts/1000";

function App() {

  const [url, setUrl] = useState();

  // TODO: 1. make function (promise version)
  // TODO: 1.1. create function
  // TODO: 1.2. inside function, call GOOD_URL api (hint: axios.get)
  // TODO: 1.3. print response to console.log()
  function promiseFcn() {
    axios.get(url)
      .then(res => {
        console.log(res);
      })
      .catch((errorMsg) => {
        console.log(errorMsg);
      })
  }

  // TODO: 2. make function (async await version)
  // TODO: 2.1. create function
  // TODO: 2.2. inside function, call GOOD_URL api (hint: axios.get)
  // TODO: 2.3. print response to console.log()
  async function asyncFcn() {
    try {
      const res = await axios.get(url);
      console.log(res);
    } catch (e) {
      console.log(e);
    }
  }

  // TODO: 3. Error handle
  // TODO: 3.2. Error handle for promise version
  // TODO: 3.3. Error handle for async await version

  function makeRandom() {
    setUrl(Math.random() > 0.5 ? GOOD_URL : BAD_URL); // Random pick use good or bad url
  }

  useEffect(() => {
    makeRandom()
    // TODO: 3.1 uncomment following line to simulate bad request
  }, []);

  return (
    <div className="App">
      {/* TODO: 1.4. bind function to button */}
      <button onClick={() => { promiseFcn(); makeRandom() }}> Promise </button>

      {/* TODO: 2.4. bind function to button */}
      <button onClick={() => { asyncFcn(); makeRandom() }}> Async Await </button>
    </div>
  );
}

export default App;
